Juste des fichiers csv représentant les POI de chaque couche de la map

# dossier bin
Contient les binaires compilés de mtsatellite ( https://bitbucket.org/s_l_teichmann/mtsatellite ) nécessaires à la création des tuiles.

* dossier debian : binaires compilés pour Debian 8 (Jessie) x86_64
* dossier fedora : binaires compilés pour Fedora 24 x86_64
* mtautocolors.txt : fichiers colors pour les mods actifs sur framinetest:30000

# dossier scripts
* map_bckp.py : script Python 2 pour backup de la map
    requiert python2-apsw / python-apsw - installable via apt-get ou dnf
    requiert tendo - installable via easy_install : https://github.com/pycontribs/tendo/blob/master/tendo/singleton.py
* map_bckp.cfg : fichier de config du script
* mapbackup.logrotate : conf logrotate pour le fichier de log
