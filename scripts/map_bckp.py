#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  map_bckp.py
#
#  Copyright 2016 fat115 <fat115@framasoft.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import apsw
import subprocess
import logging
import time
import ConfigParser
import os
import sys
# https://github.com/pycontribs/tendo/blob/master/tendo/singleton.py
from tendo import singleton

me = singleton.SingleInstance() # will sys.exit(-1) if other instance is running

config = ConfigParser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'map_bckp.cfg'))
# paths
mapfile = config.get('General', 'mapfile')
backupmapfile = config.get('General', 'backupfile')
backupfile = backupmapfile + '.temp'
colors = config.get('General', 'colors')
mtredispath = config.get('General', 'mtredispath')
mtseederpath = config.get('General', 'mtseederpath')
maptilespath = config.get('General', 'maptilespath')
logfile = config.get('General', 'logfile')

# vars
serverok = 'Server started'
mtscolopts = '-colors=' + colors
mtsoutdir = '-o=' + maptilespath

logging.basicConfig(filename = logfile, level=logging.DEBUG, format='%(asctime)s -- %(levelname)s -- %(message)s')

logging.info('debut backup')
_connection = apsw.Connection(mapfile)
_connection.setbusytimeout(10000)
destdb = apsw.Connection(backupfile)
with destdb.backup("main", _connection, "main") as backup:
    while not backup.done:
        try:
            backup.step(1000)
        except apsw.BusyError:
            logging.info('BusyError : nouvelle tentative dans 1 minute')
            time.sleep(30)
        except apsw.LockedError:
            logging.info('LockedError : nouvelle tentative dans 1 minute')
            time.sleep(30)

logging.info('fin backup')
logging.info('copie backup')
os.rename(backupfile, backupmapfile)
logging.info('fin copie backup')

logging.info('lancement mtredisalize')
mtredis = subprocess.Popen([mtredispath,'-host=localhost','-interleaved=false','-change-url=http://localhost:8808/update','-change-duration=30s','-driver=sqlite',backupmapfile], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

while True:
    output = mtredis.stdout.readline()
    if output == '':
        logging.error('erreur mtredisalize : arret')
        sys.exit('Erreur mtredisalize')
    logging.info(output.rstrip())
    if serverok in output:
        break

logging.info('serveur mtrediaslize ok => lancement mtseeder')
mtseed = subprocess.Popen([mtseederpath, mtscolopts,'-w=4','-t=true',mtsoutdir], env={'GOMAXPROCS' : '4'}, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

for line in mtseed.stdout:
    if 'Writing' in line:
        continue
    elif 'creating' in line:
        continue
    else:
        logging.warning('mtseeder : '+line.rstrip())

mtseed.wait()

logging.info('fin mtseeder')

mtredis.terminate()
logging.info('fin mtredisalize')
